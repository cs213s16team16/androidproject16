package com.example.david.lolscheduler;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RateCalculator extends AppCompatActivity {
    String acc;
    MainDbHelper db;
    EditText et1;
    EditText et2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_calculator);
        acc = (String)getIntent().getExtras().get("account");
        db = new MainDbHelper(this);
        update();
        et1 = (EditText) findViewById(R.id.editText);
        et2 = (EditText) findViewById(R.id.editText2);
        Button b1 = (Button) findViewById(R.id.button);
        b1.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      db.setIp(acc, Integer.parseInt(et1.getText().toString()));
                                      update();
                                  }
                              }
        );
        Button b2 = (Button) findViewById(R.id.button2);
        b2.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      db.setRate(acc, Double.parseDouble(et2.getText().toString()));
                                      update();
                                  }
                              }
        );
    }

    public void update() {
        TextView ci = (TextView)(findViewById(R.id.currentIp));
        int ip = db.getIp(acc);
        ci.setText("Current IP: " + ip);

        //Button btn1 = (Button)(findViewById(R.id.))

        TextView main = (TextView)findViewById(R.id.daysLeft);
        main.setText("");
        int ipNeeded = (int)getIntent().getExtras().get("ipNeeded") - db.getIp(acc);
        main.append("Total IP Required: " + ipNeeded + " IP\n");
        double rate = db.getRate(acc);
        main.append("Days Required: ");
        if (rate<ipNeeded/999) {
            main.append(" >999 Days");
        } else {
            main.append((int)ipNeeded/rate + " Days");
        }

        TextView ir = (TextView)(findViewById(R.id.ipRate));
        ir.setText("IP Rate: " + db.getRate(acc));
    }
}
