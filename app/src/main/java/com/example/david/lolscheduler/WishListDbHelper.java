package com.example.david.lolscheduler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 5/3/2016.
 */
public class WishListDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 4;
    // Database Name
    private static final String DATABASE_NAME = "wishlistManager";
    // tasks table name
    private static final String TABLE_WISHLIST = "accounts";
    // tasks Table Columns names
    //private static final String KEY_ID = "id";
    private static final String KEY_ACCOUNT_NAME = "accountName";
    private static final String KEY_CHAMPION_NAME = "champName";
    private static final String KEY_CHAMPION_PRICE = "champPrice";
    private static final String KEY_CHAMPION_STATUS ="champStatus"; //1 COMPLETE 0 INCOMPLETE
    //private static final String KEY_STATUS = "status";

    public WishListDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_WISHLIST + " ( "
                + KEY_ACCOUNT_NAME + " TEXT, " + KEY_CHAMPION_NAME + " TEXT, " + KEY_CHAMPION_PRICE
                + " INTEGER, " + KEY_CHAMPION_STATUS + " INTEGER default 0, " + " PRIMARY KEY ( "
                + KEY_ACCOUNT_NAME + ", " + KEY_CHAMPION_NAME + ") " +  ")" ;
        db.execSQL(sql);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WISHLIST);
        // Create tables again
        onCreate(db);
    }

    public void updateChampion(String accountName, String championName, int status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ACCOUNT_NAME, accountName);
        values.put(KEY_CHAMPION_NAME, championName);
        values.put(KEY_CHAMPION_STATUS, status);// task name
        // Inserting Row
        db.insert(TABLE_WISHLIST, null, values);
        db.close(); // Closing database connection
    }

   /* public void accountEntry(String accountName){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  " + KEY_CHAMPION_NAME + ", " + KEY_CHAMPION_STATUS + " FROM " + TABLE_WISHLIST + " WHERE "
                + KEY_ACCOUNT_NAME + " = " + accountName;


    }*/

    public List<String> getAllAccounts() {
        List<String> accountsList = new ArrayList<String>();
// Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_WISHLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String account;
                account = cursor.getString(1);
// Adding contact to list
                accountsList.add(account);
            } while (cursor.moveToNext());
        }
// return task list
        return accountsList;
    }
    public List<String> getAllChamps() {
        List<String> accountsList = new ArrayList<String>();
// Select All Query
        String selectQuery = "SELECT "+ KEY_CHAMPION_NAME + " FROM " + TABLE_WISHLIST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String account;
                account = cursor.getString(1);
// Adding contact to list
                accountsList.add(account);
            } while (cursor.moveToNext());
        }
// return task list
        return accountsList;
    }

    public List<WishlistItem> getAllItems(String accountName) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_WISHLIST + " WHERE "
                + KEY_ACCOUNT_NAME + "=" + '"' + accountName + '"' + ";";
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<WishlistItem> items = new ArrayList<WishlistItem>();
        if (cursor.moveToFirst()) {
            do {
                String account = cursor.getString(0);
                String champion = cursor.getString(1);
                int status = cursor.getInt(3);
                items.add(new WishlistItem(champion, account, status));
            } while (cursor.moveToNext());
        }
        return items;
    }

    public void addAccount(String accountName) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (String championName : ChampionList.getAllChampions()) {
            ContentValues values = new ContentValues();
            values.put(KEY_ACCOUNT_NAME, accountName);
            values.put(KEY_CHAMPION_NAME, championName);
            values.put(KEY_CHAMPION_PRICE, ChampionList.getIpCost(championName));
            db.insert(TABLE_WISHLIST, null, values);
        }
        db.close(); // Closing database connection
    }

    public void removeAccount(String accountName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WISHLIST, KEY_ACCOUNT_NAME + "=" + '"' + accountName + '"', null);
        db.close();
    }

    public void updateItems(List<WishlistItem> items) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (WishlistItem item : items) {
            ContentValues values = new ContentValues();
            String whereClause = KEY_ACCOUNT_NAME + "="
                    + '"' + item.account + '"'
                    + " AND " + KEY_CHAMPION_NAME + "="
                    + '"' + item.champion + '"';
                    values.put(KEY_CHAMPION_STATUS, item.status);
            db.update(TABLE_WISHLIST, values, whereClause, null);
        }
        db.close();
    }
}

