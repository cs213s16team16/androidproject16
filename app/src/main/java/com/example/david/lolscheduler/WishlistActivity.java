package com.example.david.lolscheduler;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class WishlistActivity extends AppCompatActivity {
    protected WishListDbHelper db;
    String acc;
    List<String> mChampionList;
    ListView mListView;
    SpecialAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        acc = (String)getIntent().getExtras().get("account");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        db = new WishListDbHelper(this);
        //mChampionList = db.getAllChamps();
        mChampionList = Arrays.asList(ChampionList.getAllChampions());

        mListView = (ListView)findViewById(R.id.listView2);
        mAdapter = new SpecialAdapter(this, R.layout.list_item_wishlist, db.getAllItems(acc));
        mListView.setAdapter(mAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle((String)getIntent().getExtras().get("account"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*int count = 0;
                for (WishlistItem item : mAdapter.resultsList) {
                    count += item.status;
                }
                Snackbar.make(view, String.valueOf(count), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                db.updateItems(mAdapter.resultsList);
                Intent intent = new Intent(getApplicationContext(), RateCalculator.class);
                intent.putExtra("account", acc);
                int ipNeeded = 0;
                for (WishlistItem item : mAdapter.resultsList) {
                    if (item.status==1) {
                        ipNeeded += item.cost;
                    }
                }
                intent.putExtra("ipNeeded", ipNeeded);
                startActivity(intent);
            }

        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
