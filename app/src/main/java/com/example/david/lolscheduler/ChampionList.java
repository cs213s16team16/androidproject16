package com.example.david.lolscheduler;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by David on 5/2/2016.
 */
public class ChampionList {
    static HashMap<String, Integer> list = null;

    public static void init() {
        list = new HashMap<String, Integer>();
        list.put("Ashe", 450);
        list.put("Azir", 6300);
        list.put("Caitlyn", 4800);
        list.put("Corki", 3150);
        list.put("Draven", 6300);
        list.put("Ezreal", 4800);
        list.put("Graves", 4800);
        list.put("Jayce", 6300);
        list.put("Jhin", 6300);
        list.put("Jinx", 6300);
        list.put("Kennen", 4800);
        list.put("Kindred", 6300);
        list.put("Kog'Maw", 4800);
        list.put("Lucian", 6300);
        list.put("Miss Fortune", 3150);
        list.put("Quinn", 6300);
        list.put("Sivir", 450);
        list.put("Teemo", 1350);
        list.put("Tristana", 1350);
        list.put("Twitch", 3150);
        list.put("Urgot", 3150);
        list.put("Varus", 6300);
        list.put("Vayne", 4800);

    }

    public static String[] getAllChampions() {
        if (list==null) {
            init();
        }
        return list.keySet().toArray(new String[list.size()]);
    }

    public static int getIpCost(String championName) {
        if (list==null) {
            init();
        }
        return list.get(championName);
    }
}
