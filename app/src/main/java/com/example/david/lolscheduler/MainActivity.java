package com.example.david.lolscheduler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    protected MainDbHelper db;
    List<String> mAccountsList;
    ListView mListView;
    ArrayAdapter<String> mAdapter;
    String mSelectedAccount = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new MainDbHelper(this);
        mAccountsList = db.getAllAccounts();


        ListView l = (ListView)findViewById(R.id.listView);
        mListView = l;
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mListView.setSelector(android.R.color.holo_blue_light);
                mSelectedAccount = mAccountsList.get(i);
            }
        });
        /*String[] accounts = {
                "Azjin",
                "Quinn et Val",
                "TheDaveCage"
        };*/
       Button b = (Button)findViewById(R.id.nextBtn);
        b.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     if (null== mSelectedAccount) {
                         Context context = getApplicationContext();
                         CharSequence text = "Select an Account First!";
                         int duration = Toast.LENGTH_SHORT;

                         Toast toast = Toast.makeText(context, text, duration);
                         toast.show();
                     } else {
                         Intent intent = new Intent(getApplicationContext(), WishlistActivity.class);
                         intent.putExtra("account", mSelectedAccount);
                         startActivity(intent);
                     }
                 }

            }
        );
        Button addBtn = (Button)findViewById(R.id.addbutton);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateAccountActivity.class);
                startActivity(intent);
            }
        });
        Button delBtn = (Button)findViewById(R.id.deletebutton);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null==mSelectedAccount) {
                    Context context = getApplicationContext();
                    CharSequence text = "Select an Account First!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                } else {
                    db.removeAccount(mSelectedAccount);
                    mAdapter.remove(mSelectedAccount);
                    mAdapter.notifyDataSetChanged();
                    mListView.setSelector(android.R.color.transparent);
                    mSelectedAccount = null;
                }
            }
        });
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mAccountsList);
        l.setAdapter(mAdapter);

        //((ImageView)(findViewById(R.id.test))).setImageResource(android.R.drawable.star_big_on);
    }

    /*
    public void addAccount(String account) {
        if (mAccountsList.contains(account)) {
            Context context = getApplicationContext();
            CharSequence text = "Account Already Exists!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            db.addAccount(account, 0, 0.0);
            mAdapter.add(account);
            mAdapter.notifyDataSetChanged();
        }
    }
    */
}
