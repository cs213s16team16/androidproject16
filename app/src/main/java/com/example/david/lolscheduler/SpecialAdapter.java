package com.example.david.lolscheduler;

/**
 * Created by David on 5/4/2016.
 */
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class SpecialAdapter extends ArrayAdapter<WishlistItem> {
    Context context;
    List<WishlistItem> resultsList = new ArrayList<WishlistItem>();
    int layoutResourceId;
    public SpecialAdapter(Context context, int layoutResourceId,
                                List<WishlistItem> objects) {
        super(context, layoutResourceId, objects);
        this.layoutResourceId = layoutResourceId;
        this.resultsList = objects;
        this.context = context;
    }
    /**
     * This method will DEFINe what the view inside the list view will
     * finally look like Here we are going to code that the checkbox state
     * is the status of task and check box text is the task name
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView championName = null;
        TextView ipCost = null;
        ImageButton wishlistStatus = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_wishlist,
                    parent, false);
            championName = (TextView) convertView.findViewById(R.id.list_item_wishlist_champion_name);
            ipCost = (TextView) convertView.findViewById(R.id.list_item_wishlist_ip_cost);
            wishlistStatus = (ImageButton) convertView.findViewById(R.id.list_item_wishlist_status);
            convertView.setTag(championName);
        } else {
            championName = (TextView)convertView.getTag();
            ipCost = (TextView) convertView.findViewById(R.id.list_item_wishlist_ip_cost);
            wishlistStatus = (ImageButton) convertView.findViewById(R.id.list_item_wishlist_status);
            wishlistStatus.setTag(resultsList.get(position));
        }
        //ImageButton wishlishStatus = (ImageButton) findViewById(R.id.list_item_wishlist_status);
        wishlistStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((WishlistItem)v.getTag()).status = (((WishlistItem)v.getTag()).status+1)%2 ;
                notifyDataSetChanged();
            }
        });
        WishlistItem current = resultsList.get(position);
        championName.setText(current.champion);
        ipCost.setText(String.valueOf(current.cost));
        switch (current.status) {
            case 0:
                wishlistStatus.setImageResource(android.R.drawable.star_big_off);
                break;
            case 1:
                wishlistStatus.setImageResource(android.R.drawable.star_big_on);
                break;
            default:
                // should never happen
        }
        return convertView;
    }

}

