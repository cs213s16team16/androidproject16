package com.example.david.lolscheduler;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class CreateAccountActivity extends AppCompatActivity {
    EditText mAccountNameView;
    EditText mIpAmountView;
    EditText mIpRateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create New Account");

        mAccountNameView = (EditText)findViewById(R.id.account_name);
        mIpAmountView = (EditText)findViewById(R.id.ip_amount);
        mIpRateView = (EditText)findViewById(R.id.ip_rate);

        FloatingActionButton submitBtn = (FloatingActionButton) findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean success;
                String accountName = mAccountNameView.getText().toString().trim();
                String ipAmountString = mIpAmountView.getText().toString().trim();
                String ipRateString = mIpRateView.getText().toString().trim();
                if (accountName.length()==0) {
                    Context context = getApplicationContext();
                    CharSequence text = "Please enter an account name.";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    success = false;
                } else {
                    int ipAmount;
                    double ipRate;
                    if (ipAmountString.length()==0) {
                        ipAmount = 0;
                    } else {
                        ipAmount = Integer.parseInt(ipAmountString);
                    }
                    if (ipRateString.length()==0) {
                        ipRate = 0;
                    } else {
                        ipRate = Double.parseDouble(ipRateString);
                    }
                    if (ipAmount<0 || ipRate<0) {
                        Context context = getApplicationContext();
                        CharSequence text = "IP Amount and IP Rate must both be non-negative!";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        success = false;
                    } else {
                        success = addAccount(accountName, ipAmount, ipRate);
                    }
                }
                if (success) {
                    NavUtils.navigateUpFromSameTask(getActivity());
                } else {
                    Context context = getApplicationContext();
                    CharSequence text = "Invalid account information.";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });
        FloatingActionButton cancel_action = (FloatingActionButton) findViewById(R.id.cancel_action);
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavUtils.navigateUpFromSameTask(getActivity());
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private Activity getActivity() {
        return this;
    }

    public boolean addAccount(String account, int ip, double rate) {
        MainDbHelper db = new MainDbHelper(this);
        List<String> accountsList = db.getAllAccounts();
        if (accountsList.contains(account)) {
            Context context = getApplicationContext();
            CharSequence text = "Account Already Exists!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return false;
        } else {
            db.addAccount(account, 0, 0.0);
            return true;
        }
    }

}
