package com.example.david.lolscheduler;

/**
 * Created by David on 5/2/2016.
 */
public class WishlistItem {
    String champion;
    int cost;
    String account;
   public int status;

    public WishlistItem(String champion, String account, int status){
        this.champion = champion;
        this.account = account;
        this.cost = ChampionList.getIpCost(champion);
        this.status = status;
    }
}
