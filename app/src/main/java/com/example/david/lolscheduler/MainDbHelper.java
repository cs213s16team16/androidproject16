package com.example.david.lolscheduler;

/**
 * Created by David on 5/1/2016.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MainDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    // Database Name
    private static final String DATABASE_NAME = "accountManager";
    // tasks table name
    private static final String TABLE_ACCOUNTS = "accounts";
    // tasks Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_ACCOUNT_NAME = "accountName";
    private static final String KEY_IP = "ip";
    private static final String KEY_RATE = "rateperday";

    private Context context;

    //private static final String KEY_STATUS = "status";

    public MainDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_ACCOUNTS + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_ACCOUNT_NAME + " TEXT,"
                + KEY_IP + " INTEGER, "
                + KEY_RATE + " REAL " +" ) ";
        db.execSQL(sql);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNTS);
        // Create tables again
        onCreate(db);
    }

    public void addAccount(String accountName, int ip, double rateperday) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ACCOUNT_NAME, accountName);
        values.put(KEY_IP, ip);
        values.put(KEY_RATE, rateperday);// task name
        // Inserting Row
        db.insert(TABLE_ACCOUNTS, null, values);
        db.close(); // Closing database connection
        new WishListDbHelper(context).addAccount(accountName);
    }

    public void removeAccount(String accountName) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Deleting Row
        db.delete(TABLE_ACCOUNTS, KEY_ACCOUNT_NAME + " = '" + accountName + "'", null);
        db.close(); // Closing database connection
        new WishListDbHelper(context).removeAccount(accountName);
    }

    public int getIp(String accountName){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  " + KEY_IP + " FROM " + TABLE_ACCOUNTS + " WHERE " + KEY_ACCOUNT_NAME + " = " + "\"" + accountName + "\";";
        Cursor cursor = db.rawQuery(selectQuery, null);
        int ret;
        if(cursor.moveToFirst()){
            ret = cursor.getInt(0);
        }else{
            ret = -1;
        }
        db.close();
        return ret;
    }
    public void setIp(String accountName, int ip) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_IP, ip);
        String whereClause = KEY_ACCOUNT_NAME + "=" + '"' + accountName + '"';
        db.update(TABLE_ACCOUNTS, values, whereClause, null);
        db.close();
    }
    public void setRate(String accountName, double rate) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_RATE, rate);
        String whereClause = KEY_ACCOUNT_NAME + "=" + '"' + accountName + '"';
        db.update(TABLE_ACCOUNTS, values, whereClause, null);
        db.close();
    }

    public double getRate(String accountName) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  " + KEY_RATE + " FROM " + TABLE_ACCOUNTS + " WHERE " + KEY_ACCOUNT_NAME + " = " + '"' + accountName + '"' + ";";
        Cursor cursor = db.rawQuery(selectQuery, null);
        double ret;
        if(cursor.moveToFirst()){
            ret = cursor.getDouble(0);
        }else{
            ret = -1;
        }
        db.close();
        return ret;
    }

    public List<String> getAllAccounts() {
        List<String> accountsList = new ArrayList<String>();
// Select All Query
        String selectQuery = "SELECT  " + KEY_ACCOUNT_NAME + " FROM " + TABLE_ACCOUNTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String account;
                account = cursor.getString(0);
// Adding contact to list
                accountsList.add(account);
            } while (cursor.moveToNext());
        }
// return task list
        return accountsList;
    }

}
